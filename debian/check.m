### The following was taken from file doc/spice2ocs_demo.m_in

disp ("[Diode bridge rectifier solved by DASPK]");

cd doc

outstruct = prs_spice ("rect");
x         = [0; 0; 0; 0];
t         = linspace (0, 3e-10, 60);
dmp       = .1;
pltvars   = {"Vin", "Voutlow", "Vouthigh"};
tol       = 1e-15;
maxit     = 100;

out = tst_daspk (outstruct, x, t, tol, maxit, pltvars);

tests = {"assert (size (out), [4, 60]);",
         "assert (max (out (2, :)), 0.22739, 1e-5);",
         "assert (min (out (2, :)), -4.78751, 1e-5);"};

count = 0;

for i = 1 : length (tests)
    try
        eval (tests {i});
        count += 1;
    catch
        disp (lasterror ().message);
    end_try_catch
endfor

disp (sprintf ("PASSES %d out of 3 tests", count));
